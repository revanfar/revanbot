from datetime import datetime

class DateHelper():
	def __init__(self):
		self.today = datetime.now()

	def get_date(self):
		self.datestr = datetime.strftime(self.today,"%Y%m%d")
		return self.datestr

	def get_hour(self):
		self.hourstr = datetime.strftime(self.today,"%HH%MM%SS")
		return self.hourstr
