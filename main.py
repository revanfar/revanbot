import json 
import requests
import time
import datetime
import urllib
import subprocess
import re
import logging
import os
import sqlite3
from apscheduler.schedulers.background import BackgroundScheduler
from modules.dbhelper import DBHelper
from modules.sshhelper import SSHHelper
from modules.cellhelper import CellHelper
from modules.cmdhelper import CMDHelper

version = '0.1.1218'
# TOKEN = "492739504:AAF2y61aiJRUb7_0WPMWypPILl8nDRUq8Lw" #Develooping BOT
TOKEN = "493874622:AAGFZurjMDWx0b0fxYFp1c5acM6mV8AfUOw" #Production BOT
# allowed_chatid = [-271491963,439675354] #DEVELOPING BOT
allowed_chatid = [439675354,-207514740,-226080153,-369622751] #PRODUCTION BOT
#Revanfar, Outer group, Tembilahan, dumai group 

URL_bak = "https://api.telegram.org/bot{}/".format(TOKEN)
URL = "http://149.154.167.220/bot{}/".format(TOKEN)

db = DBHelper("telebot.db")
cell = CellHelper()
cmdhelper = CMDHelper()
conn_oss = SSHHelper('10.2.81.217', 'indriasy', '')
# conn_oss1 = SSHHelper('10.2.81.217', 'baihaqif', '')
# conn_oss1 = SSHHelper('10.2.81.217', 'REMCY10', '')
scheduler = BackgroundScheduler()
#DB Connection for ipdatabase and cell update



#Error handling needed
def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content

def get_json_from_url(url):
    content = get_url(url)
    js = json.loads(content)
    return js

def get_last_chat_id_and_text(updates):
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    text = updates["result"][last_update]["message"]["text"]
    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    return (text, chat_id)

def send_message(text, chat_id):
    text = urllib.quote_plus(text)
    url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
    get_url(url)
    
def get_updates(offset=None):
    url = URL + "getUpdates?timeout=200"
    if offset:
        url += "&offset={}".format(offset)
    js = get_json_from_url(url)
    return js

def get_last_update_id(updates):
    update_ids = []
    for update in updates["result"]:
        update_ids.append(int(update["update_id"]))
    return max(update_ids)

def mainscheduling():
    log = logging.getLogger('apscheduler.executors.default')
    log.setLevel(logging.INFO)  # DEBUG

    fmt = logging.Formatter('%(levelname)s:%(name)s:%(message)s')
    h = logging.StreamHandler()
    h.setFormatter(fmt)
    log.addHandler(h)
    scheduler.add_job(sitedown_update, trigger='cron', minute="*/2", id='sitedown_update')
    # scheduler.add_job(ipdatabase_update, trigger='cron', hour='04', minute='00', id='ipdatabase_update')
    # scheduler.add_job(cellprofile_update, trigger='cron', hour='10', minute='00', id='cellprofile_update')
    # scheduler.add_job(celldb_update, trigger='cron', hour='08', minute='17', id='celldb_update')
    # scheduler.add_job(test, trigger='cron', second='*', id='test')
    scheduler.start()

def sitedown_update():
    conn_start = conn_oss.connect()

    #Sitedown Log Initialize
#============================================

    scp_cmd = "scp indriasy@10.2.81.217:/home/indriasy/dump2g3g ./log/downlog/"
    awk = '''$(awk '{$(NF-1)="" ; print $0}' ./log/downlog/dump2g3g > ./log/downlog/dump2g3g_mod)'''
    awk_cmd = "echo %s" %(awk)

    if conn_start == False:
        return 'failed to connect to OSS1'

    cl = '''rm dump2g3g
    ./down2g3g | grep POU > dump2g3g
    '''
    if not isinstance(cl, list):
            cl = cl.splitlines()

    for com in cl:
        conn_oss.do(com)

    proc1 = subprocess.Popen(scp_cmd, shell=True, stdout=subprocess.PIPE)
    proc2 = subprocess.Popen(awk_cmd, shell=True, stdout=subprocess.PIPE)

#============================================

def ipdatabase_update():
    date = datetime.datetime.now().strftime('%Y-%m-%d')
    package_dir = os.path.abspath(os.path.dirname(__file__))
    dbname = 'telebot.db'
    dbfolder = 'modules/db'
    dbpath = os.path.join(package_dir, dbfolder, dbname)
    conn = sqlite3.connect(dbpath)
    ##IPdatabase DB Initializing
    #============================================
    scp_cmd = "scp baihaqif@10.2.81.217:/home/indriasy/haniftitip/nodeb_ip/ipdatabase log/ipdatabaselog/"
    proc1 = subprocess.Popen(scp_cmd, shell=True, stdout=subprocess.PIPE)
    fname = './log/ipdatabaselog/ipdatabase'
    scp_lte_cmd = "scp baihaqif@10.2.81.217:/home/indriasy/haniftitip/nodeb_ip/ipdatabase_lte_sbt2 log/ipdatabaselog/"
    proc2 = subprocess.Popen(scp_lte_cmd, shell=True, stdout=subprocess.PIPE)
    fname_lte = './log/ipdatabaselog/ipdatabase_lte_sbt2'


    with open(fname) as f:
        content = f.readlines()

    with open(fname_lte) as fl:
        content_lte = fl.readlines()

    result = []
    for line in content:
        if len(line)>1:
            result.append(line)

    for line in content_lte:
        if len(line)>1:
            result.append(line)

    for i in range(len(result)):
        result[i] = result[i].split()


    to_db = [(date, i[0], i[1], i[3], i[4], i[5]) for i in result]
    # print to_db
    # db.ipdatabase_truncate()
    stmt = "DELETE FROM ipdatabase"
    stmt2 = "VACUUM"
    conn.execute(stmt)
    conn.execute(stmt2)
    conn.commit()
    # db.ipdatabase_additem(to_db)
    cur = conn.cursor()
    cur.executemany("INSERT INTO ipdatabase ('DATE','SITEID','IPOAM','RBSTYPE','RNC','STATUS') VALUES (?, ?, ?, ?, ?, ?);", to_db)
    conn.commit()
    conn.close()
#============================================

def cellprofile_update():

    ##Cellprofile DB Initializing
#============================================
    package_dir = os.path.abspath(os.path.dirname(__file__))
    dbname = 'telebot.db'
    dbfolder = 'modules/db'
    dbpath = os.path.join(package_dir, dbfolder, dbname)
    conn = sqlite3.connect(dbpath)
    cell.setup()
    # db.cellprofile_truncate()
    stmt = "DELETE FROM cellprofile"
    stmt2 = "VACUUM"
    # args = (item_text, )
    conn.execute(stmt)
    conn.execute(stmt2)
    conn.commit()

    # db.cellprofile_outer_truncate()


    data_all = cell.parse_logdaily()  # ALL DATA
    # db.cellprofile_additem(data_all)
    cur = conn.cursor()
    cur.executemany("INSERT INTO cellprofile ('DATE','BSC','SITE','CELL','LAC','CI','NCC_BCC','BCCH','TRX','TG','STATUS') VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", data_all)
    conn.commit()
    conn.close()
    
    # data_outer = cell.cellprofile_outer() # OUTER ONLY
    # db.cellprofile_outer_additem(data_outer)
    # print data_outer

#============================================

def msg_lookup(siteid):
    items = db.siteprofile_getdata(siteid)
    if items != []:
        message = '''
        =====SITE INFO=====
        SITE ID : %s
        SITE NAME : %s
        LATITUDE : %s
        LONGITUDE : %s
        WALI SITE : %s
        REVENUE CLASS : %s
        TP : %s
        ''' % (items[0][0],items[0][1],items[0][2],items[0][3],items[0][4],items[0][5],items[0][6])
        # print message
    else:
        message = "Site ID Not Found"
    
    return message
        # print message

def sitedown(area):
    fname = './log/downlog/dump2g3g_mod'
    with open(fname) as f:
        content = f.readlines()

    if not isinstance(content, list):
        content = content.splitlines()

    data = []
    data1 = []
    data2 = []
    message_all = ""
    message_1 = ""
    message_2 = ""

    for i in range(len(content)):
        a = content[i].split()
        for j in range(len(a)):
            if a[j] == '0':
                a.pop(j)
            else:
                continue
        for j in range(len(a)):
            if a[j] == 'Pekanbaru_Outer_1':
                data1.append(a)
            elif a[j] == 'Pekanbaru_Outer_2':
                data2.append(a)
            else:
                continue

        # print a[0]       
        data.append(a)

    data.sort()
    data1.sort()
    data2.sort()


    for i in range(len(data)): 
        message_all = message_all + "\n \n" + " ".join(str(x) for x in data[i])
    
    for i in range(len(data1)): 
        message_1 = message_1 + "\n \n" + " ".join(str(x) for x in data1[i])

    for i in range(len(data2)): 
        message_2 = message_2 + "\n \n" + " ".join(str(x) for x in data2[i])


    message_all = message_all + "\n \n" + "HD : revanfar_bot"
    message_1 = message_1 + "\n \n" + "HD : revanfar_bot"
    message_2 = message_2 + "\n \n" + "HD : revanfar_bot"
    # print message_2

    if area == '1':
        return message_1
    elif area == '2':
        return message_2
    else:
        return message_all 

def bsc_cmd(cmd,site):
# PRODUCTION
    cmdtype = 'bts'
    if site[-1] == 'g' or site[-1] == 'G':
        band = "gsm"
    elif site[-1] == 'd' or site[-1] == 'D':
        band = "dcs"
    else:
        band = "none"

    # print site
    try:
        if band != "none":
            siteid = site[:-1]
            # print siteid
            data_temp = cellprofile(siteid)
            # print data_temp
            if data_temp[1] != []:
                data = data_temp[1]
                msg = data_temp[0]
                bsc = data[0][0]
                cell_temp = []
                tg_temp = []
                tg = []
                gsm = []
                dcs = []
                cmd = cmd[1:] 

                for i in range(len(data)):
                    if data[i][4] != 'HALTED':
                        cell_temp.append(data[i][2])
                        tg_temp.append(data[i][3])
                    else:
                        pass

                for i in range(len(cell_temp)):
                    if cell_temp[i][-1] == 'A' or cell_temp[i][-1] == 'B' or cell_temp[i][-1] == 'C':
                        gsm.append(i)
                    elif cell_temp[i][-1] == 'E' or cell_temp[i][-1] == 'F' or cell_temp[i][-1] == 'G' :
                        dcs.append(i)
                    else:
                        pass
                
                if band == 'gsm' and gsm != [] and tg != '':
                    regex = ['A','B','C']
                    for i in gsm:
                        tg.append(tg_temp[i])
                        sitename = data[i][1]
                    tg = list(set(tg))

                elif band == 'dcs' and dcs != [] and tg != '':
                    regex = ['E','F','G']
                    for i in dcs:
                        tg.append(tg_temp[i])
                        sitename = data[i][1]
                    tg = list(set(tg))
                
                else:
                    print "Wrong band"

                if cmd != "" and bsc != "" and tg != [] and sitename != "":
                    message = cmdhelper.generate(cmdtype=cmdtype,cmd=cmd,bsc=bsc,tg=tg,sitename=sitename,siteid=siteid,regex=regex)
                    # print bsc
                else:
                    message = "Ga ada %s disini mah" %(band)
            else:
                message = "Site ID not found"
        else:
            message = "Incomplete Site ID.\nExample:\n/rxcdp bls045g\n/rxcdp ssi199d"
    except Exception as e:
        print e
    
    # print message
    # print result
    return message
    
## DEBUGGING
    # print tg
    # print data
    # print tg_temp
    # print result
    # cmd.transmissiontodb()    
            
def nodeb_cmd(cmd,site):
    cmdtype = 'nodeb'
    siteid = ''
    cmd = cmd[1:]
    # print cmd
    # print site
    if site != '':
        site= site.upper()
        if len(site)==7:
            siteid = site[:-1]
        elif len(site) < 7:
            siteid = site
        elif len(site)>8:
            pass

        # print siteid
        if siteid != '':
            search_data = '%'+siteid+'%'
            rnc_regex = 'RN%'
            # print search_data
            result = db.ipdatabase_search(search_data,rnc_regex)
            # print result

            # result = result[0]
            # print result
            if result != []:
                result = result[0]
                rnc = result[0]
                sitename = result[1]
                ipoam = result[2]
                rbstype = result[3]
                message = cmdhelper.generate(cmdtype=cmdtype,cmd=cmd,rnc=rnc,sitename=sitename,ipoam=ipoam,rbstype=rbstype)
                # print rnc,sitename,ipoam,rbstype
            else:
                message = "Site ID not found"
        else:
            message = "Huh, did you write a wrong siteid?"

    print message
    return message

def enodeb_cmd(cmd,site):
    message = ''
    cmdtype = 'enodeb'
    siteid = ''
    cmd = cmd[1:]
    # print cmd
    # print site
    if site != '':
        site= site.upper()
        if len(site)==7:
            siteid = site[:-1]
        elif len(site) < 7:
            siteid = site
        elif len(site)>8:
            pass

        # print siteid
        if siteid != '':
            search_data = '%'+siteid+'%'
            rnc_regex = 'LTE%'
            # print search_data
            result = db.ipdatabase_search(search_data,rnc_regex)
            # print result

            # result = result[0]
            # print result
            if result != []:
                result = result[0]
                rnc = result[0]
                sitename = result[1]
                ipoam = result[2]
                rbstype = result[3]
                message = cmdhelper.generate(cmdtype=cmdtype,cmd=cmd,rnc=rnc,sitename=sitename,ipoam=ipoam,rbstype=rbstype)
                # print rnc,sitename,ipoam,rbstype
            else:
                message = "Site ID not found"
        else:
            message = "Huh, did you write a wrong siteid?"

    # print message
    return message

def cellprofile(data):
    if data != "":

        data = data.upper()
        if len(data)==7:
            data = data[:-1]
        elif len(data) < 7:
            pass
        elif len(data)>8:
            result = "Error Site ID"

        if data[0:3] == 'BKG':
            data = data.replace('BKG','BG3')
        elif data[0:3] == 'TBH':
            data = data.replace('TBH','TH5')
        else:
            pass

        # print data
        search_data = data[0:2]+'_'+data[3:]+'_'
        # print search_data
        result = db.cellprofile_search(search_data)
        # print result
        
        if result != []:
            message = "BSC SITE CELL LAC CI TG STATUS"
            for i in range(len(result)):
                message = message + "\n" + " ".join(x for x in result[i])
        else:
            message = "Site ID Not Found"

    else:
        message = "Incomplete command.\nExample:\n/cellprofile ssi199"
    
    return message, result


def message_routing(updates):
    try:
        for update in updates["result"]:
            now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            date = datetime.datetime.now().strftime('%Y-%m-%d')
            hour = datetime.datetime.now().strftime('%H:%M')
            text = update["message"]["text"]
            chat = update["message"]["chat"]["id"]
            # print chat
            requestor_id = update["message"]["from"]["id"]
            requestor_username = update["message"]["from"]["first_name"]
            # requestor_lastname = update["message"]["from"]["last_name"]
            # requestor_username = requestor_firstname+requestor_lastname
            # group_id = chat
            # group_name = update["message"]["chat"]["title"]

            # userlog = [date,hour,requestor_id,requestor_username,text]
            cmdtype = text.split()
            to_db_conversationlog = [(date,hour,requestor_id,requestor_username,text,cmdtype[0])]
            # print requestor_id
            # print requestor_firstname
            # print to_db_conversationlog
            db.convlog_additem(to_db_conversationlog)

            if chat in allowed_chatid:
                # print requestor_username
                split_text = text.split()
                # print split_text[0]

                if (len(split_text) == 1):
                    if (split_text[0] == '/help'):
                        help_msg = '''
                        BOT Version : %s
    ===General Commands===
    /status Check if BOT is Alive!
    /help Show all available commands
    /lookup Lookup general information by site ID
    /sitedown Show all site down under Outer PKU

    ===2G Commands===
    /cellinfo2g Show NE Info (Config/Blocked)(2G)
    /rxasp Show NE Alarm (2G)
    /txcheck Show Transmission status (2G)
    /loading2g Loading TG on specified site ID (2G)

    ===3G Commands===
    /traffic3g Show total users on specified site ID (3G)
    /cellinfo3g Show cell availability on specified site ID (3G)
    /loading3g Loading DUW/DUS on specified site ID (3G)

    ===4G Commands===
    /traffic4g Show total users on specified site ID (4G)
    /cellinfo4g Show cell availability on specified site ID (4G)
    /loading4g Loading BB on specified site ID (4G)

                        '''%(version)
# status - Check if BOT is Alive!
# help - Show all available commands
# lookup - Lookup general information by site ID
# sitedown - Show all site down under Outer PKU
# cellinfo2g - Show NE Info (Config/Blocked)(2G)
# rxasp - Show NE Alarm (2G)
# txcheck - Show Transmission status (2G)
# loading2g - Loading TG on specified site ID (2G)
# traffic3g - Show total users on specified site ID (3G)
# cellinfo3g - Show cell availability on specified site ID (3G)
# loading3g - Loading DUW/DUS on specified site ID (3G)
# traffic4g - Show total users on specified site ID (4G)
# cellinfo4g - Show cell availability on specified site ID (4G)
# loading4g - Loading BB on specified site ID (4G)

                        response_msg = help_msg

                    elif (split_text[0] == '/sitedown'):
                        response_msg = sitedown('3')

                    elif (split_text[0] == '/status'):
                        response_msg = "Bot is alive!"

                    elif (split_text[0] == '/txcheck'):
                        response_msg = "Show Transmission status (2G).\nExample:\n/txcheck ssi199g"

                    # elif (split_text[0] == '/checktg'):
                    #     response_msg = "Show all available cell by site ID.\nExample:\n/cellprofile bls045g"

                    elif (split_text[0] == '/cellinfo2g'):
                        response_msg = "Show NE Info (Config/Blocked)(2G).\nExample:\n/cellinfo2g bls045d"

                    elif (split_text[0] == '/rxasp'):
                        response_msg = "Show NE Alarm (2G).\nExample:\n/rxasp ssi501g"

                    elif (split_text[0] == '/loading2g'): 
                        response_msg = "Loading TG on specified site ID(2G).\nExample:\n/loadingtg bls055d"
                    
                    # elif (split_text[0] == '/loadingcf'): 
                    #     response_msg = "Loading CF on specified site ID(2G).\nExample:\n/loadingcf ssi501d"

                    elif (split_text[0] == '/traffic3g'): 
                        response_msg = "Show total users on specified site ID(3G).\nExample:\n/traffic bls338"

                    elif (split_text[0] == '/cellinfo3g'): 
                        response_msg = "Show cell availability on specified site ID(3G).\nExample:\n/cellinfo bls657"

                    elif (split_text[0] == '/loading3g'): 
                        response_msg = "Loading DUW/DUS on specified site ID(3G).\nExample:\n/loading3g ssi227"

                    elif (split_text[0] == '/traffic4g'): 
                        response_msg = "Loading DUW/DUS on specified site ID(4G).\nExample:\n/traffic4g bls055"

                    elif (split_text[0] == '/loading4g'): 
                        response_msg = "Loading BB on specified site ID(4G).\nExample:\n/loading4g ssi187"

                    # elif (split_text[0] == '/cellinfo4g'): 
                    #     response_msg = "Loading DUW/DUS on specified site ID(3G).\nExample:\n/cellinfo4g ssi227"

                    else:
                        response_msg = "Oops, wrong command. Say what? Please consider /help to know what command you can do. Ciao "

                elif (len(split_text) > 1):


                    if (split_text[0] == '/lookup'):
                        response_msg = msg_lookup(split_text[1].upper())

                    elif (split_text[0] == '/sitedown'):
                        response_msg = sitedown(split_text[1])

                    # elif (split_text[0] == '/checktg'):
                    #     response_msg = cellprofile(split_text[1])
                    #     response_msg = response_msg[0]

                    elif (split_text[0] == '/cellinfo2g'): 
                        response_msg = bsc_cmd(split_text[0],split_text[1]) #band,cmd,site

                    elif (split_text[0] == '/rxasp'): 
                        response_msg = bsc_cmd(split_text[0],split_text[1]) #band,cmd,site

                    elif (split_text[0] == '/txcheck'): 
                        response_msg = bsc_cmd(split_text[0],split_text[1]) #band,cmd,site

                    elif (split_text[0] == '/loading2g'): 
                        response_msg = bsc_cmd(split_text[0],split_text[1]) #band,cmd,site

                    # elif (split_text[0] == '/loadingcf'): 
                    #     response_msg = bsc_cmd(split_text[0],split_text[1]) #band,cmd,site

                    elif (split_text[0] == '/traffic3g'):
                        response_msg = nodeb_cmd(split_text[0],split_text[1]) 

                    elif (split_text[0] == '/cellinfo3g'):
                        response_msg = nodeb_cmd(split_text[0],split_text[1])

                    # elif (split_text[0] == '/temperature'):
                    #     response_msg = nodeb_cmd(split_text[0],split_text[1]) 
                    
                    elif (split_text[0] == '/loading3g'):
                        response_msg = nodeb_cmd(split_text[0],split_text[1]) 
                        # response_msg = "Loading 3G sedang ada problem di OSS."
                    elif (split_text[0] == '/traffic4g'):
                        response_msg = enodeb_cmd(split_text[0],split_text[1]) 

                    elif (split_text[0] == '/loading4g'):
                        response_msg = enodeb_cmd(split_text[0],split_text[1]) 

                  
                    else:
                        response_msg = "Command not found"
                        
                else:
                    # response_msg = "Hello"
                    # send_message(response_msg, chat)
                    pass

                # print response_msg
            else:
                # print "Prohibited"
                response_msg = "You're not allowed to chat me! Please contact my creator @revanfar"
                # print chat
            
        send_message(response_msg, chat)
        # else:
            
    except Exception as e:
        # for update in updates["result"]:
        #     chat = update["message"]["chat"]["id"]
        #     response_msg = "Halo bang, selamat join! mohon bimbingannya karena awak masih bodoh, tapi awak akan belajar terus ko ;)"
        #     send_message(response_msg, chat)
        print e

def main():
    #Debugging Session
    # print sitedown(1) 
    # db_debug()
    # msg_lookup("BKG015")
    # cell.setup()
    # db.setup()
    # db.cellprofile_truncate()
    # cellprofile('BKG093G')
    # bsc_cmd('/loadingtg','bls173g')
    # nodeb_cmd('/traffic3g','bls601')
    # initialize_db()
    # cellprofile_update()
    # ipdatabase_update()
    # enodeb_cmd('/loading4g','BLS145')

    ## Production Session
    last_update_id = None
    try:
        mainscheduling()
        while True:
            now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            print "%s \t Updating Message" % (now)
            updates = get_updates(last_update_id)
            if len(updates["result"]) > 0:
                last_update_id = get_last_update_id(updates) + 1
                message_routing(updates)
            time.sleep(2)
    except KeyboardInterrupt,SystemExit:
        print('Exitting by Ctrl+C')
        scheduler.shutdown()
   

if __name__ == '__main__':
    main()
