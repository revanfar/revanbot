import sqlite3

class DBHelper(object):
    def __init__(self, dbname):
        self.dbname = dbname
        self.conn = sqlite3.connect(dbname)

    def setup(self):
        stmt = "CREATE TABLE IF NOT EXISTS items (description text)"
        self.conn.execute(stmt)
        self.conn.commit()

    def add_item(self, item_text):
        stmt = "INSERT INTO items (description) VALUES (?)"
        args = (item_text, )
        self.conn.execute(stmt, args)
        self.conn.commit()

    def cellprofile_truncate(self):
        stmt = "DELETE FROM cellprofile"
        stmt2 = "VACUUM"
        # args = (item_text, )
        self.conn.execute(stmt)
        self.conn.execute(stmt2)
        self.conn.commit()

    def cellprofile_outer_truncate(self):
        stmt = "DELETE FROM cellprofile_outer"
        stmt2 = "VACUUM"
        # args = (item_text, )
        self.conn.execute(stmt)
        self.conn.execute(stmt2)
        self.conn.commit()

    def cellprofile_additem(self,data):
        cur = self.conn.cursor()
        cur.executemany("INSERT INTO cellprofile ('DATE','BSC','SITE','CELL','LAC','CI','NCC_BCC','BCCH','TRX','TG','STATUS') VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", data)
        self.conn.commit()
        # self.conn.close()

    def cellprofile_outer_additem(self,data):
        cur = self.conn.cursor()
        cur.executemany("INSERT INTO cellprofile_outer ('DATE','BSC','SITE','CELL','LAC','CI','NCC_BCC','BCCH','TRX','TG','STATUS') VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", data)
        self.conn.commit()
        # self.conn.close()

    def cellprofile_search(self,data):
        stmt = "SELECT BSC,SITE,CELL,LAC,CI,TG,STATUS  FROM cellprofile_outer WHERE CELL LIKE '%s' ORDER BY CELL ASC" %(data)
        return [x for x in self.conn.execute(stmt)]
        # self.conn.close()

    def siteprofile_getdata(self,siteid):
        stmt = "SELECT * FROM general_info WHERE site_id = '%s'" %(siteid)
        # print stmt
        return [x[1:] for x in self.conn.execute(stmt)]
        # self.conn.close()
        