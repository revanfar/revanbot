import subprocess
import csv
import os
from datehelper import DateHelper

class CellHelper:
    def __init__(self):
        # self.date = DateHelper().get_date()
        self.date = '20180801'

    def setup(self):
        filename = "dapot_hasil_2G_{}.txt".format(self.date)
        path = "/home/indriasy/datapot2G/LogDapotDaily/"
        logdir = "./log/dapotlog/"
        self.localpath = os.path.join(logdir,filename)
        scp_cmd = "scp indriasy@10.2.81.217:%s%s %s" %(path,filename,logdir)
        subprocess.Popen(scp_cmd, shell=True, stdout=subprocess.PIPE)
             

    def parse_logdaily(self):
        reader = csv.DictReader(
            open(self.localpath),
            fieldnames=(self.date,'BSC','SITE','CELL','NEID','LAC','CI','NCC_BCC','BCCH','TRX','TG','STATUS'),
        )
        next(reader)
        whitespace_cleaning = (dict((k, v.strip()) for k, v in row.items() if v) for row in reader)

        to_db = [(i[self.date], i['BSC'], i['SITE'], i['CELL'], i['LAC'], i['CI'], i['NCC_BCC'], i['BCCH'], i['TRX'], i['TG'][3:], i['STATUS']) for i in whitespace_cleaning]
        # print to_db
        return to_db

    def cellprofile_outer(self):
        data = self.parse_logdaily()
        arr = []
        result = []
        for i in data:
            arr.append(i)

        for i in range(len(arr)):
            if arr[i][1] == 'BVSSI2' or arr[i][1] == 'BVSSI1':
                result.append(arr[i])
            else:
                pass

        to_db = [(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9][3:], i[10]) for i in result]
        return to_db
        