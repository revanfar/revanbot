import os
import re
import subprocess
from operator import itemgetter
from datehelper import DateHelper
from sshhelper import SSHHelper
from dbhelper import DBHelper


class CMDHelper():
	def __init__(self):
		self.db = DBHelper('telebot.db')
		# self.remotepath = "/home/baihaqif/revtest"
		self.user = "baihaqif"
		# self.user = "REMCY10"
		self.remotepath = "/home/%s/revtest" % (self.user)
		self.datehelper = DateHelper()
		self.conn = SSHHelper('10.2.81.217', self.user, '')
		self.date = self.datehelper.get_date()
		self.hour = self.datehelper.get_hour()
		# self.buffering = 0

	# def generate(self,cmdtype,bsc,tg,sitename,siteid,regex):
	# (cmdtype=cmdtype,cmd=cmd,bsc=bsc,tg=tg,sitename=sitename,siteid=siteid,regex=regex)
	def generate(self,**kwargs):
		cmdtype = kwargs['cmdtype']

		if cmdtype == 'bts':
			siteid = kwargs['siteid']
			regex = kwargs['regex']
			cmd = kwargs['cmd']
			bsc = kwargs['bsc']
			tg = kwargs['tg']
			sitename = kwargs['sitename']
			sitename = sitename.replace('#','')

			# print siteid,regex,cmd,bsc,tg,sitename

			siteid = siteid.upper()
			dbres = []
			arr_temp = []
			tcuid = []
			siteidstr = []
			txtype = {}

			osscmd = ""
			if cmd == 'txcheck':
				for text in regex:
					siteidstr.append(siteid[0:2]+'_'+siteid[3:]+text)
				
				dbres = self.db.cmdtxcheck_search(siteidstr)

				for i in range(len(dbres)):
					txtype[dbres[i][2]] = (dbres[i][3],dbres[i][4])

			for i in tg:
				if cmd == 'cellinfo2g':
					osscmd = osscmd+"rxcdp:mo=rxotg-%s;" %(i)
				elif cmd == 'rxasp':
					osscmd = osscmd+"rxasp:mo=rxotg-%s;" %(i)
				elif cmd == 'txcheck':
					if txtype[i][0] == 'SCM':
						if bsc[1] != 'V':
							osscmd = osscmd+"dtstp:dip=rblt%s;"%(i)  
						else:
							osscmd = osscmd+"dtstp:dip=%set;"%(i)
					elif txtype[i][0] == 'IPM':
						osscmd = osscmd+"rrptp:pstu=%s;"%(txtype[i][1])
					# else:

				elif cmd == 'loading2g':
					osscmd = osscmd+'''rxbli:mo=rxotg-%s,subord,force;      
			rxese:mo=rxotg-%s,subord;
			rxesi:mo=rxotg-%s,subord;
			rxble:mo=rxotg-%s,subord;'''%(i,i,i,i)
			# 	elif cmd == 'loadingcf':
			# 		osscmd = osscmd+'''rxbli:mo=rxocf-%s,subord,force;      
			# rxese:mo=rxocf-%s,subord;
			# rxesi:mo=rxocf-%s,subord;
			# rxble:mo=rxocf-%s,subord;'''%(i,i,i,i)
				elif cmd == 'rxtcp':
					osscmd = osscmd+'''rxtcp:moty=rxotg;'''
				elif cmd == 'rrsgp':
					osscmd = osscmd+'''rrsgp:scgr=all;'''
				else:
					print "Wrong mode"
					break
				osscmd = osscmd+"\n"

			# print cmd

			template = '''@ORDERED("OFF")\n@CONNECT("%s")\n%s
			'''%(bsc,osscmd)
			remotepath = os.path.join(self.remotepath, bsc, self.date)
			localpath,filename = self.local_logging(bsc,template,sitename)
			# print template
			if remotepath != '' and localpath != '' and filename != '':
				remotefile = self.sending(localpath,remotepath,filename)
				if remotefile != False:
					result = self.executing(cmdtype=cmdtype,remotefile=remotefile,osscmd='',ipoam='')
					if result != '' or result != []:
						split_result = result.splitlines()
						separator = '|'
						tgstr = separator.join(tg)
						message_temp = ''
						message = '%s %s result: \n'%(sitename,cmd)
						if cmd == 'cellinfo2g':
							for line in split_result:
								if re.search(tgstr, line):
									if re.search(r'RXOTX.*$|RXORX.*$',line):
										txt = re.sub(r'NONE', " ", line) 
										message_temp = message_temp + "\n" + txt
							
							result_temp = []
							res_arr = []
						
							res = message_temp.splitlines()
							for line in res:
								a = line.split()
								result_temp.append(a)

							for i in range(1,len(result_temp)):
								res_arr.append(result_temp[i][0])
								res_arr.append(result_temp[i][1])

							for x in res_arr:
								if x == 'CONFIG' or x == 'BLOCKED':
									message = message + x + "\n"
								else:	
									message = message + x + "|"

						elif cmd == 'rxasp':
							for line in split_result:
								if re.search(tgstr, line):
									if re.search(r'RX.*$',line):
										message = message + "\n" + line

						elif cmd == 'txcheck':
							for line in split_result:
								if re.search(r'DIP.*|IEX.*|WAIT.*|TCU.*|ACT.*|PSTU.*',line):
									message = message + "\n" + line

						elif cmd == 'loading2g':
							message = "TG has been loaded successfully!"

						# elif cmd == 'loadingcf':
						# 	message = "CF has been loaded successfully!"

						else:
							message = "Command not found!"
				
					else:
						message = "Null result"
				else:
					message = "Can't connect or remote File Error: %s"%(remotefile)
			else:
				message = "Remote path error: %s, Local path error: %s, filename error: %s "%(remotepath,localpath,filename)
		
		elif cmdtype == 'nodeb':
			rnc = kwargs['rnc']
			sitename = kwargs['sitename']
			ipoam = kwargs['ipoam']
			rbstype = kwargs['rbstype']
			self.rbstype = rbstype
			cmd = kwargs['cmd']
			message = ''
			message_temp = ''

			if rnc != '' and sitename != '' and ipoam != '' and rbstype != '':
				self.conn_start = self.conn.connect()
				if self.conn_start == False:
				    return 'failed to connect to OSS'

				cl = "ping %s"% (ipoam)

				if not isinstance(cl, list):
					cl = cl.splitlines()

				for com in cl:
					status_temp = self.conn.do(com)

				# print status_temp
				if re.search(r'.*alive',status_temp):
					status = 'ALIVE'
				else:
				    status = 'HEARTBEAT_FAILURE'

				# print status

				if status == 'ALIVE':
					if rbstype == '#BB#':
						credential = 'username=rbs,password=rbs'
					elif rbstype == '##':
						credential = ''
					else:
						message = "Wrong RBS type"
					
					template = "%s\t%s"%(ipoam,credential)

					if cmd == 'traffic3g':
						osscmd = 'lt all;hget radio no'
					elif cmd == 'cellinfo3g':
						osscmd = 'lt all;lst cell'
					# elif cmd == 'temperature':
					# 	if rbstype == '##':
					# 		osscmd = 'cab'
					# 	elif rbstype == '#BB#':
					# 		osscmd = 'cabx'
					elif cmd == 'loading3g':
						if rbstype == '##':
							osscmd = 'lt all;acc Equipment=1,Subrack=1,Slot=1,PlugInUnit=1 manualRestart;3;6;restart;'
						elif rbstype == '#BB#':
							osscmd = 'lt all;acc FieldReplaceableUnit=1 restartUnit;2;6;restart;'

					if template != '' and osscmd != '':
						remotepath = os.path.join(self.remotepath, rnc, self.date)
						localpath,filename = self.local_logging(rnc,template,sitename)

						# print remotepath,localpath,filename
						# print template
						if remotepath != '' and localpath != '' and filename != '':
							remotefile = self.sending(localpath,remotepath,filename)
							if remotefile != False:
								result = self.executing(cmdtype=cmdtype,remotefile=remotefile,osscmd=osscmd,ipoam=ipoam)
								if result != '' or result != []:
									split_result = result.splitlines()
									message = "%s\t%s's result:"%(sitename,cmd)
									if cmd == 'traffic3g':
										if rbstype == '##':
											for line in split_result:
												regex = re.search(r'(^S).*=(.*),(C).*=(.*),.*(RadioLinks=)(.?) (.+?) (.*)',line)
												if regex:
													message = message + "\n" + regex.group(1)+regex.group(2)+regex.group(3)+regex.group(4)+" "+regex.group(7)+" users"
												
										elif rbstype == '#BB#':
											for line in split_result:
												regex = re.search(r'(NodeBLocalCell=)(.*),RadioLinks=(.?) (.+?) (.*)',line)
												if regex:
													message = message + "\n" + regex.group(2)+" "+regex.group(4)+" users"


									elif cmd == 'cellinfo3g':
										if rbstype == '##':
											for line in split_result:
												regex = re.search(r'(ENABLED|DISABLED).*(RbsLocalCell=)(.*).*$',line)
												if regex:
													message = message + "\n" + regex.group(3)+" "+regex.group(1)

										elif rbstype == '#BB#':
											for line in split_result:
												regex = re.search(r'(UNLOCKED|LOCKED).*(ENABLED|DISABLED).*(NodeBLocalCell=)(.*).*$',line)
												if regex:
													message = message + "\n" + regex.group(4)+" "+regex.group(2)

									# elif cmd == 'temperature':
									# 	if rbstype == '##':
									# 		for line in split_result:
									# 			if re.search(r'DUW',line):
									# 				message_temp = line.split()
									# 		message = message_temp[2]+"|FAULTY="+message_temp[3]+"|OPER="+message_temp[4]+"|TEMP="+message_temp[11]


										elif rbstype == '#BB#':
											temp = ''
											for line in split_result:
												regex_temp = re.search(r'([0-9][0-9][C])',line)
												regex_stat = re.search(r'(DUS5201)(.*)?(OFF|ON).*(OFF|ON)',line)
												if regex_temp:
													if temp == '':
														temp = regex_temp.group(0)
													else:
														pass
													# print regex_temp.group(0)
												elif regex_stat:
													message = message + regex_stat.group(1) +"|FAULTY="+ regex_stat.group(3) +"|OPER="+ regex_stat.group(4) 

											message = message +"|TEMP=" +temp
											# else:
											# 	pass
									elif cmd == 'loading3g':
										# if rbstype == '##':
										for line in split_result:
											if re.search(r'attempted.*',line):
												message = message + line
										# elif rbstype == '#BB#':


					# print template,osscmd

				else:
					message = "NodeB Heartbeat Failure detected!"
			# print rnc,sitename,ipoam,rbstype

		elif cmdtype == 'enodeb':
			rnc = kwargs['rnc']
			sitename = kwargs['sitename']
			ipoam = kwargs['ipoam']
			rbstype = kwargs['rbstype']
			self.rbstype = rbstype
			cmd = kwargs['cmd']
			message = ''
			message_temp = ''

			if rnc != '' and sitename != '' and ipoam != '' and rbstype != '':
				self.conn_start = self.conn.connect()
				if self.conn_start == False:
				    return 'failed to connect to OSS'

				cl = "ping %s"% (ipoam)

				if not isinstance(cl, list):
					cl = cl.splitlines()

				for com in cl:
					status_temp = self.conn.do(com)

				# print status_temp
				if re.search(r'.*alive',status_temp):
					status = 'ALIVE'
				else:
				    status = 'HEARTBEAT_FAILURE'

				# print status

				if status == 'ALIVE':
					if rbstype == '#BB#':
						credential = 'username=rbs,password=rbs'
					else:
						message = "Wrong RBS type"
					template = "%s\t%s"%(ipoam,credential)

					if cmd == 'traffic4g':
						osscmd = 'lt all;ue print -admitted'
					elif cmd == 'loading4g':
						osscmd = 'lt all;acc FieldReplaceableUnit=1 restartUnit;2;6;restart;'
				# 	elif cmd == 'temperature':
				# 		if rbstype == '##':
				# 			osscmd = 'cab'
				# 		elif rbstype == '#BB#':
				# 			osscmd = 'cabx'

					if template != '' and osscmd != '':
						remotepath = os.path.join(self.remotepath, rnc, self.date)
						localpath,filename = self.local_logging(rnc,template,sitename)
						# print remotepath,localpath,filename
						# print template
						if remotepath != '' and localpath != '' and filename != '':
							remotefile = self.sending(localpath,remotepath,filename)
							if remotefile != False:
								result = self.executing(cmdtype=cmdtype,remotefile=remotefile,osscmd=osscmd,ipoam=ipoam)
								# print result
								if result != '' or result != []:
									split_result = result.splitlines()
									message = "%s\t%s's result:"%(sitename,cmd)
									if cmd == 'traffic4g':
										for i in range(len(split_result)):
											if split_result[i] == 'CellId  #UE:s  #Bearers  ':
												start = i
												
											elif split_result[i] == 'coli>':
												end = i
										# print start,end
										while start < end:
											message = message + "\n" + split_result[start] 
											start += 1

									elif cmd == 'loading4g':
									# if rbstype == '##':
										for line in split_result:
											if re.search(r'attempted.*',line):
												message = message + line
									# elif rbstype == '#BB#':
				else:
					message = "eNodeB Heartbeat Failure detected!"
		else:
			message = "Command type not found, probably LTE"

		# print message
		return message

	def local_logging(self,bscrnc,template,sitename):
		filename = self.hour+"_"+sitename
		localpath = "log/cmdlog/%s/%s/%s"%(bscrnc,self.date,filename)
		directory = os.path.dirname(localpath)

		if not os.path.exists(directory):
		    os.makedirs(directory)

		# print "Writting to file %s" %(localpath)

		file = open(localpath,'w')
		file.write(template)
		file.close()

		return localpath,filename
		# print directory

	def sending(self,localpath,remotepath,filename):
		self.conn_start = self.conn.connect()
		if self.conn_start == False:
		    return 'failed to connect to OSS'

		cl = "mkdir -p %s"% (remotepath)

		if not isinstance(cl, list):
			cl = cl.splitlines()

		for com in cl:
			execute = self.conn.do(com)

		status = True
		if status:
			scp_cmd = "scp %s %s@10.2.81.217:%s" % (localpath,self.user,remotepath)
			proc = subprocess.Popen(scp_cmd, shell=True, stdout=subprocess.PIPE)
			remotefile = os.path.join(remotepath,filename)
			print "Sending file %s to OSS1: Success" %(localpath)
			return remotefile
			# return True
		else:
			print "Sending file to OSS1: Failed"
			return False

	# def executing(self,cmdtype,remotefile,osscmd):
	def executing(self,**kwargs):
		cmdtype = kwargs['cmdtype']
		remotefile = kwargs['remotefile']
		osscmd = kwargs['osscmd']
		ipoam = kwargs['ipoam']

		# print cmdtype,remotefile,osscmd,ipoam

		self.conn_start = self.conn.connect()

		if self.conn_start == False:
		    return 'failed to connect to OSS'

		if cmdtype == 'bts':
			cl = "/opt/ericsson/bin/ops_nui -file %s"% (remotefile)
			# print remotefile
			if not isinstance(cl, list):
				cl = cl.splitlines()

			for com in cl:
				execute = self.conn.do(com)
				
		elif cmdtype == 'nodeb':
			ipdatabase_name = 'ipdatabase'
			ipdatabase_path = os.path.join(self.remotepath,ipdatabase_name)
			rbs_file = remotefile
			log_file = remotefile+'_result'

			# print ipdatabase_path,rbs_file,log_file
			if self.rbstype == '##':
				cl = "/opt/ericsson/amos/moshell/mobatch -o -v %s %s '%s' %s"%(ipoam,rbs_file,osscmd,log_file)
			elif self.rbstype == '#BB#':
				cl = "/opt/ericsson/amos/moshell/mobatch -o -v %s %s '%s' %s"%(ipoam,rbs_file,osscmd,log_file)
			else:
				pass

			execute = self.conn.do(cl)

		elif cmdtype == 'enodeb':
			ipdatabase_name = 'ipdatabase_lte_sbt2'
			ipdatabase_lte_path = os.path.join(self.remotepath,ipdatabase_name)
			rbs_file = remotefile
			log_file = remotefile+'_result'
			# print cmdtype,remotefile,osscmd,ipoam
			# print ipdatabase_path,rbs_file,log_file
			
			if self.rbstype == '#BB#':
				cl = "/opt/ericsson/amos/moshell/mobatch -o -v %s %s '%s' %s"%(ipoam,rbs_file,osscmd,log_file)
			else:
				pass

			execute = self.conn.do(cl)
		# print cl
		return execute

	def transmissiontodb(self):
		bsc = ['BVSSI1','BVSSI2','BBLS1']
		osscmd = "rrsgp:scgr=all;"
		folder = 'rrsgplog'


		for text in bsc:
			localpath = "log/cmdlog/%s/"%(folder)

			template = '''@ORDERED("OFF")\n@CONNECT("%s")\n%s
			'''%(text,osscmd)

			filename = text+"_"+self.date
			remotepath = os.path.join(self.remotepath,folder)
			# remotefile = os.path.join(remotepath,filename)
			localpath = os.path.join(localpath,filename)
			resultname = localpath+"_result"
			file = open(localpath,'w')
			file.write(template)
			file.close()
			
			remotefile = self.sending(localpath,remotepath,filename)
			# print remotefile
			result = self.executing(remotefile)

			resultfile = open(resultname,'w')
			resultfile.write(result)
			resultfile.close()

			file = open(resultname,'r')
			f = file.readlines()
			result_ipm = []
			result_scm = []
			for line in f:
				if re.search(r'IPM.*', line):
					txt = re.sub(r'4096|OFF', " ", line)
					result_ipm.append(txt.split())

			for line in f:
				if re.search(r'SCM.*',line):
					result_scm.append(line.split())

			for i in range(len(result_ipm)):
				result_ipm[i][0] = int(result_ipm[i][0])
				result_ipm[i].pop(4)
				result_ipm[i].pop(3)

			for i in range(len(result_scm)):
				result_scm[i][0] = int(result_scm[i][0])
				result_scm[i].append('')

			result_combined = result_ipm + result_scm
			result = sorted(result_combined,key=itemgetter(0))
			to_db = [(text,i[0], i[1], i[2]) for i in result]
			self.db.cmdrrsgp_additem(to_db)


