import sqlite3
import os.path


class DBHelper(object):
    def __init__(self, dbname):
        package_dir = os.path.abspath(os.path.dirname(__file__))
        # package_dir = '/home/revanfar/pywork/bot_release/beta0.3/modules'
        dbfolder = 'db'
        dbpath = os.path.join(package_dir, dbfolder, dbname)
        self.conn = sqlite3.connect(dbpath)

    # def add_item(self, item_text):
    #     stmt = "INSERT INTO items (description) VALUES (?)"
    #     args = (item_text, )
    #     self.conn.execute(stmt, args)
    #     self.conn.commit()

    def convlog_additem(self,data):
        cur = self.conn.cursor()
        cur.executemany("INSERT INTO conversationlog ('DATE','HOUR','USERID','USERNAME','FULLCMD','CMDTYPE') VALUES (?, ?, ?, ?, ?, ?);", data)
        self.conn.commit()

    def cellprofile_truncate(self):
        # cur = self.conn.cursor()
        stmt = "DELETE FROM cellprofile"
        stmt2 = "VACUUM"
        # args = (item_text, )
        self.conn.execute(stmt)
        self.conn.execute(stmt2)
        self.conn.commit()

    # def cellprofile_outer_truncate(self):
    #     stmt = "DELETE FROM cellprofile_outer"
    #     stmt2 = "VACUUM"
    #     self.conn.execute(stmt)
    #     self.conn.execute(stmt2)
    #     self.conn.commit()

    def ipdatabase_truncate(self):
        # cur = self.conn.cursor()
        stmt = "DELETE FROM ipdatabase"
        stmt2 = "VACUUM"
        self.conn.execute(stmt)
        self.conn.execute(stmt2)
        self.conn.commit()

    def cmdrrsgp_additem(self,data):
        cur = self.conn.cursor()
        stmt = "DELETE FROM cellprofile_outer"
        stmt2 = "VACUUM"
        self.conn.execute(stmt)
        self.conn.execute(stmt2)
        self.conn.commit()
        cur.executemany("INSERT INTO rrsgp ('BSC','TG','TYPE','TCUID') VALUES (?, ?, ?, ?);", data)
        self.conn.commit()

    def cellprofile_additem(self,data):
        cur = self.conn.cursor()
        cur.executemany("INSERT INTO cellprofile ('DATE','BSC','SITE','CELL','LAC','CI','NCC_BCC','BCCH','TRX','TG','STATUS') VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", data)
        self.conn.commit()
        # self.conn.close()

    # def cellprofile_outer_additem(self,data):
    #     cur = self.conn.cursor()
    #     cur.executemany("INSERT INTO cellprofile_outer ('DATE','BSC','SITE','CELL','LAC','CI','NCC_BCC','BCCH','TRX','TG','STATUS') VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", data)
    #     self.conn.commit()
        # self.conn.close()

    def ipdatabase_additem(self,data):
        cur = self.conn.cursor()
        cur.executemany("INSERT INTO ipdatabase ('SITEID','IPOAM','RBSTYPE','RNC','STATUS') VALUES (?, ?, ?, ?, ?);", data)
        self.conn.commit()

    def ipdatabase_search(self,data,rnc):
        stmt = "SELECT RNC,SITEID,IPOAM,RBSTYPE FROM ipdatabase WHERE SITEID LIKE '%s' AND RNC LIKE '%s'"%(data,rnc)
        return [x for x in self.conn.execute(stmt)]

    def cellprofile_search(self,data):
        stmt = "SELECT BSC,SITE,CELL,TG,STATUS  FROM cellprofile WHERE CELL LIKE '%s' ORDER BY CELL ASC" %(data)
        return [x for x in self.conn.execute(stmt)]
        # self.conn.close()

    def siteprofile_getdata(self,data):
        stmt = "SELECT * FROM siteinfo WHERE site_id = '%s'" %(data)
        # print stmt
        return [x[1:] for x in self.conn.execute(stmt)]
        # self.conn.close()

    def cmdtxcheck_search(self,data):
        stmt = "SELECT cellprofile.BSC,cellprofile.CELL,rrsgp.TG,rrsgp.TYPE,rrsgp.TCUID FROM rrsgp JOIN cellprofile ON rrsgp.BSC = cellprofile.BSC and rrsgp.TG = cellprofile.TG WHERE CELL LIKE '%s' OR CELL LIKE '%s' OR CELL LIKE '%s' ORDER BY CELL ASC" %(data[0],data[1],data[2])
        return [x for x in self.conn.execute(stmt)]
        