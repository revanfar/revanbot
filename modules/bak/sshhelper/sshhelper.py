import paramiko, json
#import vyos_parser as parser

class SSHHelper(object):
	"""
	Attributes:
		host (str): remote target ip address
		username (str): remote target username
		password (str): remote target password
		port (int): port for ssh connection
		client (paramiko.SSHClient): SSHClient instantiation
	"""


	def __init__(self, host, username, password, port=22):
		self.host = host
		self.username = username
		self.password = password
		self.port = port
		self.client = False


	def connect(self):
		"""
		initialize and start connection to ssh target.

		Raise:
			'BadHostKeyException': when host ssh key is missing or wrong
			'AuthenticationException': when authentication is failed
			'SSHException': paramiko SSH exception
		"""

		# print 'Info:', 'connecting', self.host, '...'
		try:
			self.client = paramiko.SSHClient()

			#if host key is missing, it will add the key automatically
			self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			self.client.load_system_host_keys()

			#start connection to remote target
			self.client.connect(self.host, port=self.port, username=self.username, password=self.password)
			# print 'Info:', self.host, 'connected'
			return True
		except (BadHostKeyException, AuthenticationException, SSHException) as e:
			#print 'Warn:', self.host, 'failed to connect'
			print 'Warn', e
			return False
		#print 'ok'


	def do(self, command):
		"""
		helper for command execution to remote target, and listen reponse from execution.
		"""

		#checking if client is connected before
		if self.client==False:
			return 'client is not connected'
		else:
			#execute ssh command and catch response to stdout or stderr
			stdin, stdout, stderr = self.client.exec_command(command)
			err = stderr.read()
			out = stdout.read()
			log = ''
			
			#if there some error catched
			if len(err)>0:
				log += 'error: '+err+'\n'
				print err

			#add response of command execution to temporary log
			if len(out)>0:
				log += 'info: '+out+'\n'
				print out
			return log


	def disconnect(self):

		#checking if client is connected before
		if self.client==False:
			return 'client not connect'
		else:
			print 'Info:', self.host, 'disconnected'

			#close the client, and set value to False
			self.client.close()
			self.client = False
			
			return True



