import datetime
import subprocess
import csv

class CellHelper:
    def __init__(self):
        today = datetime.date.today()
        self.todaystr = datetime.datetime.strftime(today,"%Y%m%d")

    def setup(self):
        self.filename = "dapot_hasil_2G_{}.txt".format(self.todaystr)
        path = "/home/indriasy/datapot2G/LogDapotDaily/"
        scp_cmd = "scp indriasy@10.2.81.217:%s%s ." %(path,self.filename)
        subprocess.Popen(scp_cmd, shell=True, stdout=subprocess.PIPE)
        

    def parse_logdaily(self):
        reader = csv.DictReader(
            open(self.filename),
            fieldnames=(self.todaystr,'BSC','SITE','CELL','NEID','LAC','CI','NCC_BCC','BCCH','TRX','TG','STATUS'),
        )
        # with open(self.filename,'rb') as file: # `with` statement available in 2.5+
            # dr = csv.DictReader(file) # comma is default delimiter
        next(reader)
        whitespace_cleaning = (dict((k, v.strip()) for k, v in row.items() if v) for row in reader)
            # print whitespace_cleaning
        # for i in whitespace_cleaning:
        #     print i
        to_db = [(i[self.todaystr], i['BSC'], i['SITE'], i['CELL'], i['LAC'], i['CI'], i['NCC_BCC'], i['BCCH'], i['TRX'], i['TG'], i['STATUS']) for i in whitespace_cleaning]
        return to_db

    def cellprofile_outer(self):
        data = self.parse_logdaily()
        # print data
        arr = []
        result = []
        for i in data:
            arr.append(i)

        # print arr[0][1]
        for i in range(len(arr)):
            if arr[i][1] == 'BVSSI2' or arr[i][1] == 'BVSSI1' or arr[i][1] == 'BPWG2' or arr[i][1] == 'BBLS1':
                result.append(arr[i])
            else:
                pass

        to_db = [(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10]) for i in result]
        # print to_db
        return to_db
        # print result
        